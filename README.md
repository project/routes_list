## Routes List

Module provides a simple dashboard with a list of all available routes
in the system and access information.

It's useful for quick overview of permissions configuration and
security check to ensure no hidden URLs are active with full access
to anyone.

### How to use

Install and enable module, like any other contrib module.

Go to Reports > Routes list

Don't forget to configure permissions to 'Routes list' page.

### Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
